import React from 'react';

//Navigation V.5
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';

// Screen
import EmployeeForm from './src/screens/EmployeeForm';
import EmployeeList from './src/screens/EmployeeList';

const Stack = createStackNavigator();

function StackNav(props) {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="EmployeeForm" headerMode={'none'}>
        <Stack.Screen name="EmployeeForm" component={EmployeeForm} />
        <Stack.Screen name="EmployeeList" component={EmployeeList} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default StackNav;
