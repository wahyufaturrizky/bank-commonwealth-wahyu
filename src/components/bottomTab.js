import * as React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Alert,
  Linking,
} from 'react-native';
import {CommonActions} from '@react-navigation/native';
import IconSimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import IconIonicons from 'react-native-vector-icons/Ionicons';
import IconMaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import IconFeather from 'react-native-vector-icons/Feather';
import AsyncStorage from '@react-native-community/async-storage';

function bottomTab(props) {
  return (
    <View
      style={[
        styles.shadow,
        {
          backgroundColor: '#FDFEFF',
          flexDirection: 'row',
          justifyContent: 'space-around',
          alignItems: 'center',
        },
      ]}>
      <TouchableOpacity
        onPress={() =>
          Alert.alert(
            '',
            'Hire Wahyu Fatur Rizki, If you want unlock this feature',
            [
              {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
              {
                text: 'Ok',
                onPress: () =>
                  Linking.openURL('https://gitlab.com/wahyufaturrizky').catch(
                    err => console.error('An error occurred', err),
                  ),
              },
            ],
            {
              cancelable: false,
            },
          )
        }>
        <View style={{alignItems: 'center'}}>
          <IconSimpleLineIcons name="home" size={30} color="#949494" />
          <Text>Home</Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() =>
          Alert.alert(
            '',
            'Hire Wahyu Fatur Rizki, If you want unlock this feature',
            [
              {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
              {
                text: 'Ok',
                onPress: () =>
                  Linking.openURL('https://gitlab.com/wahyufaturrizky').catch(
                    err => console.error('An error occurred', err),
                  ),
              },
            ],
            {
              cancelable: false,
            },
          )
        }>
        <View style={{alignItems: 'center'}}>
          <IconFeather name="youtube" size={30} color="#949494" />
          <Text>Live</Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity>
        <View style={{alignItems: 'center'}}>
          <IconIonicons name="md-chatbubbles-sharp" size={30} color="#1F99D3" />
          <Text>Diskusi</Text>
        </View>
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() =>
          Alert.alert(
            '',
            'Hire Wahyu Fatur Rizki, If you want unlock this feature',
            [
              {
                text: 'Cancel',
                onPress: () => console.log('Cancel Pressed'),
                style: 'cancel',
              },
              {
                text: 'Ok',
                onPress: () =>
                  Linking.openURL('https://gitlab.com/wahyufaturrizky').catch(
                    err => console.error('An error occurred', err),
                  ),
              },
            ],
            {
              cancelable: false,
            },
          )
        }>
        <View style={{alignItems: 'center'}}>
          <IconMaterialCommunityIcons
            name="account"
            size={30}
            color="#949494"
          />
          <Text>Profil</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

export default bottomTab;

const styles = StyleSheet.create({
  shadow: {
    shadowRadius: 2,
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 12,
  },
});
