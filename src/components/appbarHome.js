/* eslint-disable */

import React, {useEffect, useState} from 'react';
import {Appbar} from 'react-native-paper';
import {Image, View, StyleSheet, Dimensions} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {color} from 'react-native-reanimated';

function appbarHome(props) {
  let title = '';

  if (props.params.route != null) {
    title = props.params.route.params.title;
  }

  const {height, width} = Dimensions.get('window');

  return (
    <Appbar.Header
      style={[
        styles.shadow,
        {
          backgroundColor: 'white',
          width: '100%',
          height: 60,
          position: 'relative',
          top: 0,
        },
      ]}>
      <Appbar.BackAction
        onPress={() => {
          props.params.navigation.goBack();
        }}
      />

      <Appbar.Content titleStyle={{fontSize: 14}} title={title} />
    </Appbar.Header>
  );
}

export default appbarHome;

const styles = StyleSheet.create({
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.2,
    shadowRadius: 1.41,

    elevation: 2,
  },
});
