/* eslint-disable */
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  RefreshControl,
  Modal,
  StyleSheet,
  Image,
  Linking,
} from 'react-native';
import {TextInput, List} from 'react-native-paper';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import SplashScreen from 'react-native-splash-screen';

// Components
import Appbar from '../../components/appbarHome';
import Loading from '../../components/loading';

import {URL} from '../../utils/global';

// Typography
import {iOSUIKit, material, materialColors} from 'react-native-typography';

function EmployeeList(props) {
  const [state, setState] = useState({
    dataEmployees: [],
    refreshing: false,
    modalEditEmployeVisible: false,
    nameEmployee: null,
    amountSalary: null,
    age: null,
    idEmployee: null,
  });

  const {
    dataEmployees,
    refreshing,
    modalEditEmployeVisible,
    nameEmployee,
    amountSalary,
    age,
    idEmployee,
  } = state;

  const [loading, setLoading] = useState(true);

  const {height, width} = Dimensions.get('window');

  const URL_GET_EMPLOYEES = URL + 'employees';

  useEffect(() => {
    setLoading(true);

    let requestOptions = {
      method: 'GET',
      redirect: 'follow',
    };

    fetch(URL_GET_EMPLOYEES, requestOptions)
      .then((response) => response.text())
      .then((result) => {
        let dataParse = JSON.parse(result);
        console.log('fetch data employees', dataParse);
        alert(`${dataParse.message}`);
        setState({
          ...state,
          dataEmployees: dataParse.data,
        });
        setLoading(false);
      })
      .catch((error) => {
        console.log('error', error);
        alert(
          `Gagal memuat data karyawan \n\nServer error from dummy.restapiexample.com`,
        );
        setLoading(false);
      });
  }, []);

  const _onRefresh = () => {
    setState({...state, refreshing: true});

    let requestOptions = {
      method: 'GET',
      redirect: 'follow',
    };

    fetch(URL_GET_EMPLOYEES, requestOptions)
      .then((response) => response.text())
      .then((result) => {
        let dataParse = JSON.parse(result);
        console.log('fetch data employees', dataParse);
        alert(`${dataParse.message}`);
        setState({
          ...state,
          dataEmployees: dataParse.data,
          refreshing: false,
        });
      })
      .catch((error) => {
        setState({...state, refreshing: false});
        console.log('error', error);
        alert(
          `Gagal memuat data karyawan \n\nServer error from dummy.restapiexample.com`,
        );
      });
  };

  const handleChange = (value, name) => {
    setState({
      ...state,
      [name]: value,
    });
  };

  const deleteDataEmployee = (data) => {
    setLoading(true);

    let urlencoded = new URLSearchParams();

    let requestOptions = {
      method: 'DELETE',
      body: urlencoded,
      redirect: 'follow',
    };

    fetch(URL + `delete/${data.id}`, requestOptions)
      .then((response) => response.text())
      .then((result) => {
        let dataParse = JSON.parse(result);
        console.log('fetch data employees', dataParse);
        alert(`${dataParse.message}`);
        setLoading(false);
      })
      .catch((error) => {
        console.log('error', error);
        alert(
          `Gagal menghapus data karyawan \n\nServer error from dummy.restapiexample.com`,
        );
        setLoading(false);
      });
  };

  const editDataEmployee = () => {
    setLoading(true);

    console.log('------ DATA INPUT ------');
    console.log('nameEmployee', nameEmployee);
    console.log('amountSalary', amountSalary.toString());
    console.log('age', age);
    console.log('idEmployee', idEmployee);
    console.log('------ DATA INPUT ------');

    let myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');

    let raw = JSON.stringify({
      name: nameEmployee,
      salary: amountSalary,
      age: age,
    });

    let requestOptions = {
      method: 'PUT',
      headers: myHeaders,
      body: raw,
      redirect: 'follow',
    };

    fetch(URL + `update/${idEmployee}`, requestOptions)
      .then((response) => response.text())
      .then((result) => {
        console.log('succes edit data', result);
        alert(`Successfully! Record has been updated.`);
        setLoading(false);
      })
      .catch((error) => {
        console.log('error', error);
        alert(
          `Gagal edit data karyawan \n\nServer error from dummy.restapiexample.com`,
        );
        setLoading(false);
      });
  };

  return (
    <View style={{flex: 1, height: '100%', backgroundColor: '#FFC100'}}>
      <Appbar params={props} />
      <ScrollView
        refreshControl={
          <RefreshControl refreshing={refreshing} onRefresh={_onRefresh} />
        }
        keyboardShouldPersistTaps="always">
        <View
          style={{
            padding: 15,
            backgroundColor: '#FFC100',
            marginTop: height * 0.01,
            flex: 1,
          }}>
          <Text
            style={[
              iOSUIKit.title3Emphasized,
              {textAlign: 'center', marginBottom: 12},
            ]}>
            List Data Employees
          </Text>
          {dataEmployees?.length > 0 ? (
            dataEmployees?.map((key, index) => {
              const {
                id,
                employee_name,
                employee_salary,
                employee_age,
                profile_image,
              } = key;
              return (
                <View
                  style={{
                    backgroundColor: 'white',
                    borderRadius: 20,
                    marginBottom: 24,
                    padding: 12,
                  }}
                  key={index}>
                  <List.Item
                    left={(props) => (
                      <List.Icon {...props} icon="account-box" />
                    )}
                  />
                  <List.Item title={`ID : ${id}`} />
                  <List.Item title={`Name : ${employee_name}`} />
                  <List.Item title={`Salary : Rp ${employee_salary}`} />
                  <List.Item title={`Age : ${employee_age}`} />
                  <View
                    style={{
                      justifyContent: 'space-between',
                      flexDirection: 'row',
                      alignItems: 'center',
                      marginTop: 24,
                    }}>
                    <TouchableOpacity
                      style={{
                        width: '40%',
                        alignSelf: 'center',
                        backgroundColor: 'white',
                        marginBottom: height * 0.03,
                      }}
                      onPress={() =>
                        setState({
                          ...state,
                          modalEditEmployeVisible: true,
                          nameEmployee: employee_name,
                          amountSalary: employee_salary,
                          age: employee_age,
                          idEmployee: id,
                        })
                      }>
                      <LinearGradient
                        start={{x: 0, y: 0}}
                        end={{x: 1, y: 1}}
                        colors={['#000000', '#000000', '#000000']}
                        style={{
                          padding: 10,
                          justifyContent: 'center',
                          alignItems: 'center',
                          borderRadius: 6,
                        }}>
                        <Text
                          style={{
                            fontSize: 14,
                            textAlign: 'center',
                            color: 'white',
                          }}>
                          EDIT
                        </Text>
                      </LinearGradient>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={{
                        width: '40%',
                        alignSelf: 'center',
                        backgroundColor: 'white',
                        marginBottom: height * 0.03,
                      }}
                      onPress={() => deleteDataEmployee(key)}>
                      <LinearGradient
                        start={{x: 0, y: 0}}
                        end={{x: 1, y: 1}}
                        colors={['#000000', '#000000', '#000000']}
                        style={{
                          padding: 10,
                          justifyContent: 'center',
                          alignItems: 'center',
                          borderRadius: 6,
                        }}>
                        <Text
                          style={{
                            fontSize: 14,
                            textAlign: 'center',
                            color: 'white',
                          }}>
                          DELETE
                        </Text>
                      </LinearGradient>
                    </TouchableOpacity>
                  </View>
                </View>
              );
            })
          ) : (
            <Text style={{textAlign: 'center'}}>
              Server error from dummy.restapiexample.com
            </Text>
          )}
        </View>
      </ScrollView>
      <Text style={{textAlign: 'center', marginBottom: 12}}>
        Swipe down to refresh
      </Text>
      <TouchableOpacity
        onPress={() => {
          Linking.openURL('https://gitlab.com/wahyufaturrizky').catch((err) =>
            console.error('An error occurred', err),
          );
        }}>
        <Text
          style={{
            textAlign: 'center',
            marginVertical: 12,
            color: materialColors.blackTertiary,
          }}>
          Click to see Author: Wahyu Fatur Rizki
        </Text>
      </TouchableOpacity>

      {/* ------ [START MODAL LOGIN] ------ */}
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalEditEmployeVisible}
        onRequestClose={() => {
          setState({
            ...state,
            modalEditEmployeVisible: false,
          });
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <TouchableOpacity
              onPress={() => {
                setState({
                  ...state,
                  modalEditEmployeVisible: false,
                });
              }}>
              <Image
                style={{
                  borderColor: '#16212D',
                  borderWidth: 2,
                  borderRadius: 10,
                  marginBottom: 20,
                }}
                source={require('../../assets/images/close.png')}
              />
            </TouchableOpacity>
            <Text style={[styles.modalText, material.title]}>
              Edit Employee!
            </Text>
            <Text
              style={[
                styles.modalText,
                material.subheading,
                {
                  color: materialColors.blackTertiary,
                },
              ]}>
              Masukkan data yang ingin di ubah.
            </Text>

            <View style={{flex: 1}}>
              <TextInput
                label="Fill full name"
                keyboardType="default"
                value={nameEmployee}
                mode="outlined"
                onChangeText={(val) => handleChange(val, 'nameEmployee')}
                style={{
                  backgroundColor: 'white',
                  marginBottom: 40,
                }}
                theme={{
                  colors: {primary: '#07A9F0', underlineColor: 'transparent'},
                }}
              />

              <TextInput
                label="Fill sallary"
                keyboardType="number-pad"
                value={`${amountSalary}`}
                mode="outlined"
                onChangeText={(val) => handleChange(val, 'amountSalary')}
                style={{
                  backgroundColor: 'white',
                  marginBottom: 40,
                }}
                theme={{
                  colors: {primary: '#07A9F0', underlineColor: 'transparent'},
                }}
              />

              <TextInput
                label="Fill age"
                keyboardType="number-pad"
                value={`${age}`}
                mode="outlined"
                onChangeText={(val) => handleChange(val, 'age')}
                style={{
                  backgroundColor: 'white',
                }}
                theme={{
                  colors: {primary: '#07A9F0', underlineColor: 'transparent'},
                }}
              />
            </View>

            <TouchableOpacity
              style={{
                backgroundColor: 'white',
              }}
              onPress={editDataEmployee}>
              <LinearGradient
                start={{x: 0, y: 0}}
                end={{x: 1, y: 1}}
                colors={['#000000', '#000000', '#000000']}
                style={{
                  padding: 10,
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 6,
                }}>
                <Text
                  style={{fontSize: 14, textAlign: 'center', color: 'white'}}>
                  SAVE
                </Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
      {/* ------ [END MODAL LOGIN] ------ */}

      {/* ----- [SET LOADING PAGE] ----- */}
      {loading && <Loading />}
      {/* ----- [SET LOADING PAGE] ----- */}
    </View>
  );
}

export default EmployeeList;

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    margin: 20,
    backgroundColor: 'white',
    borderRadius: 20,
    padding: 35,
    // alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  modalText: {
    marginBottom: 20,
    textAlign: 'left',
  },
});
