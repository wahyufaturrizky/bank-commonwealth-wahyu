/* eslint-disable */
import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Linking,
} from 'react-native';
import {TextInput} from 'react-native-paper';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import SplashScreen from 'react-native-splash-screen';

import Loading from '../../components/loading';

import {URL} from '../../utils/global';

// Typography
import {iOSUIKit} from 'react-native-typography';

function EmployeeForm(props) {
  const [state, setState] = useState({
    nameEmployee: null,
    amountSalary: null,
    age: null,
  });

  const {nameEmployee, amountSalary, age} = state;

  const [loading, setLoading] = useState(true);

  const {height, width} = Dimensions.get('window');

  const URL_POST = URL + 'create';

  useEffect(() => {
    SplashScreen.hide();
    setLoading(false);
  }, []);

  const handleChange = (value, name) => {
    setState({
      ...state,
      [name]: value,
    });
  };

  const addDataEmployee = async () => {
    let checkDataEmpty = [
      {name: 'nama pegawai', value: nameEmployee},
      {name: 'total gaji', value: amountSalary},
      {name: 'usia', value: age},
    ];

    let emptyField = checkDataEmpty.find((field) => field.value === null);

    if (!emptyField) {
      setLoading(true);

      console.log('------ DATA INPUT ------');
      console.log('nameEmployee', nameEmployee);
      console.log('amountSalary', amountSalary);
      console.log('age', age);
      console.log('------ DATA INPUT ------');

      let myHeaders = new Headers();
      myHeaders.append('Content-Type', 'application/json');

      var raw = JSON.stringify({
        name: nameEmployee,
        salary: amountSalary,
        age: age,
      });

      var requestOptions = {
        method: 'POST',
        headers: myHeaders,
        body: raw,
        redirect: 'follow',
      };

      fetch(URL_POST, requestOptions)
        .then((response) => response.text())
        .then((result) => {
          let dataParse = JSON.parse(result);
          console.log('succes add data', dataParse);
          alert(`${dataParse.message}`);
          setLoading(false);
        })
        .catch((error) => {
          console.log('error', error);
          alert(
            `Gagal nambah data karyawan \n\nServer error from dummy.restapiexample.com`,
          );
          setLoading(false);
        });
    } else {
      alert(`field ${emptyField.name} wajib di isi`);
    }
  };

  return (
    <View style={{flex: 1, height: '100%', backgroundColor: '#FFC100'}}>
      <ScrollView keyboardShouldPersistTaps="always">
        <View
          style={{
            padding: 15,
            backgroundColor: '#FFC100',
            marginTop: height * 0.2,
            flex: 1,
          }}>
          <Text
            style={[
              iOSUIKit.title3Emphasized,
              {textAlign: 'center', marginBottom: 24},
            ]}>
            Form Basic Employee
          </Text>
          <TextInput
            label="Fill full name"
            keyboardType="default"
            value={nameEmployee}
            mode="outlined"
            onChangeText={(val) => handleChange(val, 'nameEmployee')}
            style={{
              width: '90%',
              alignSelf: 'center',
              backgroundColor: 'white',
              borderRadius: 10,
              marginBottom: height * 0.04,
            }}
            theme={{
              colors: {primary: '#07A9F0', underlineColor: 'transparent'},
            }}
          />

          <TextInput
            label="Fill sallary"
            keyboardType="number-pad"
            value={amountSalary}
            mode="outlined"
            onChangeText={(val) => handleChange(val, 'amountSalary')}
            style={{
              width: '90%',
              alignSelf: 'center',
              backgroundColor: 'white',
              borderRadius: 10,
              marginBottom: height * 0.04,
            }}
            theme={{
              colors: {primary: '#07A9F0', underlineColor: 'transparent'},
            }}
          />

          <TextInput
            label="Fill age"
            keyboardType="number-pad"
            value={age}
            mode="outlined"
            onChangeText={(val) => handleChange(val, 'age')}
            style={{
              width: '90%',
              alignSelf: 'center',
              backgroundColor: 'white',
              borderRadius: 10,
              marginBottom: height * 0.04,
            }}
            theme={{
              colors: {primary: '#07A9F0', underlineColor: 'transparent'},
            }}
          />

          <TouchableOpacity
            style={{
              width: '90%',
              alignSelf: 'center',
              backgroundColor: 'white',
              marginBottom: height * 0.03,
            }}
            onPress={addDataEmployee}>
            <LinearGradient
              start={{x: 0, y: 0}}
              end={{x: 1, y: 1}}
              colors={['#000000', '#000000', '#000000']}
              style={{
                padding: 10,
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 6,
              }}>
              <Text style={{fontSize: 14, textAlign: 'center', color: 'white'}}>
                SUBMIT
              </Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </ScrollView>
      <TouchableOpacity
        onPress={() => {
          Linking.openURL('https://gitlab.com/wahyufaturrizky').catch((err) =>
            console.error('An error occurred', err),
          );
        }}>
        <Text
          style={{
            textAlign: 'center',
            marginVertical: 12,
            color: materialColors.blackTertiary,
          }}>
          Click to see Author: Wahyu Fatur Rizki
        </Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={{
          width: '100%',
          alignSelf: 'center',
          backgroundColor: 'white',
        }}
        onPress={() =>
          props.navigation.navigate('EmployeeList', {title: 'Emlpoyee List'})
        }>
        <LinearGradient
          start={{x: 0, y: 0}}
          end={{x: 1, y: 1}}
          colors={['#0956C6', '#0879D8', '#07A9F0']}
          style={{
            padding: 15,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{fontSize: 14, textAlign: 'center', color: 'white'}}>
            SHOW ALL EMPLOYEES
          </Text>
        </LinearGradient>
      </TouchableOpacity>
      {/* ----- [SET LOADING PAGE] ----- */}
      {loading && <Loading />}
      {/* ----- [SET LOADING PAGE] ----- */}
    </View>
  );
}

export default EmployeeForm;
